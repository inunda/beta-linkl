import React from "react";
import root from "react-shadow";
import styles from "!raw-loader!../styled.css";

import http from "https";
import axios from "axios";
import { linklcall, validFunc } from "../utils/Apis";
import { myVariables } from "../utils/GlobalVariables";

import InputMask from "react-input-mask";
import SuccessTab from "./Success";

export default class ScheduleTab extends React.Component {
  constructor() {
    super();
    this.state = {
      userName: "",
      userPhone: "",
      daySel: "",
      hourSel: "",
      success: false
    };
    this.userSchedule = this.userSchedule.bind(this);
    this.daySelect = this.daySelect.bind(this);
    this.hourSelect = this.hourSelect.bind(this);
  }

  componentDidMount() {
    this.getHour();
    this.getUserInfo();
  }

  getName(e) {
    const getUserName = e.target.value;

    if (
      (getUserName.length < 5 && getUserName !== null) ||
      getUserName !== ""
    ) {
      this.setState({
        userName: getUserName
      });
    }
  }

  getPhone(e) {
    const getUserPhone = e.target.value.replace(/[{()}]/g, "").replace("-", "");

    if ((getUserPhone > 11 && getUserPhone !== null) || getUserPhone !== "") {
      this.setState({
        userPhone: getUserPhone
      });
    }
  }

  daySelect(event) {
    const userDaySel = String(event.target.value);
    this.setState({ daySel: userDaySel });
  }

  hourSelect(event) {
    const userHourSel = String(event.target.value);
    this.setState({ hourSel: userHourSel });
  }

  getUserInfo() {
    const req = http.request(
      validFunc,
      function(res) {
        let chunks = [];

        res.on("data", function(chunk) {
          chunks.push(chunk);
        });

        res.on(
          "end",
          function() {
            let body = Buffer.concat(chunks);
            let userinfo = JSON.parse(body.toString());

            let datefmtInit = userinfo.horarioInicial.split(":");
            let dateHourInit = datefmtInit[0];
            let dateMinInit = datefmtInit[1];

            let datefmtEnd = userinfo.horarioFinal.split(":");
            let dateHourEnd = datefmtEnd[0];
            let dateMinEnd = datefmtEnd[1];

            let hourLength = dateHourEnd - dateHourInit;

            let hrAlmocoInitfmt = userinfo.horarioAlmocoInicial.split(":");
            let hrAlmocoEndfmt = userinfo.horarioAlmocoFinal.split(":");

            let hrAlmocoInit = Number(hrAlmocoInitfmt[0]);
            let hrAlmocoEnd = Number(hrAlmocoEndfmt[0]);

            let hrArray = [];

            for (let i = 0; i <= hourLength; i++) {
              hrArray.push(dateHourInit++);

              if (hrArray[i] === hrAlmocoInit) {
                hrArray[i] = "Almoço";
              } else if (hrArray[i] === hrAlmocoEnd) {
                hrArray[i] = "Almoço";
              } else {
                hrArray[i] = hrArray[i] + ":00";
              }
            }

            this.setState({
              data: hrArray
            });
          }.bind(this)
        );
      }.bind(this)
    );

    req.write(JSON.stringify({ token: myVariables[1] }));
    req.end();
  }

  async getHour() {
    try {
      const { ...response } = await axios.get(
        "http://worldclockapi.com/api/json/utc/now"
      );

      let atualHour = response.data.currentDateTime.toString();
      let atualWeekDay = response.data.dayOfTheWeek.toString();
      let tomorrowcv = atualHour
        .slice(0, 10)
        .split("-")
        .reverse();
      let tomorrowfmt = Number(tomorrowcv[0]) + 1;
      let dayonefmt = Number(tomorrowcv[0]) + 2;
      let daytwofmt = Number(tomorrowcv[0]) + 3;

      if (tomorrowfmt < 10) {
        tomorrowfmt = "0" + tomorrowfmt;
      } else {
        tomorrowfmt = tomorrowfmt;
      }

      if (dayonefmt < 10) {
        dayonefmt = "0" + dayonefmt;
      } else {
        dayonefmt = dayonefmt;
      }

      if (daytwofmt < 10) {
        daytwofmt = "0" + daytwofmt;
      } else {
        daytwofmt = daytwofmt;
      }

      let monthfmt = tomorrowcv[1];
      let yearfmt = tomorrowcv[2];

      let dayonect = [String(dayonefmt), monthfmt, yearfmt];
      let daytwoct = [String(daytwofmt), monthfmt, yearfmt];
      let tomorrowct = [String(tomorrowfmt), monthfmt, yearfmt];

      let tomorrow = tomorrowct.toString().replace(/,/g, "/");
      let dayone = dayonect.toString().replace(/,/g, "/");
      let daytwo = daytwoct.toString().replace(/,/g, "/");

      let atualDay = atualHour
        .slice(0, 10)
        .split("-")
        .reverse()
        .toString()
        .replace(/,/g, "/");
      let atualHr = atualHour.slice(11, atualHour.length - 1).split(":");
      let atualHrH = Number(atualHr[0]) - 3;
      let atualHrM = Number(atualHr[1]);
      if (atualHrM < 10) {
        atualHrM = "0" + atualHrM;
      } else {
        atualHrM = atualHrM;
      }
      let autalHnM = atualHrH + ":" + atualHrM;

      this.setState({
        atualDay,
        tomorrow,
        dayone,
        daytwo,
        autalHnM,
        atualWeekDay
      });
    } catch (error) {
      console.error(error);
    }
  }

  userSchedule() {
    const {
      userName,
      userPhone,
      value,
      autalHnM,
      daySel,
      hourSel
    } = this.state;

    var req = http.request(
      linklcall,
      function(res) {
        var chunks = [];
        res.on("data", function(chunk) {
          chunks.push(chunk);
        });
        res.on(
          "end",
          function() {
            this.setState({
              success: true
            });
            setTimeout(
              function() {
                this.setState({
                  success: false
                });
              }.bind(this),
              2000
            );
          }.bind(this)
        );
      }.bind(this)
    );
    req.write(
      '{"codigo":"' +
        myVariables[1] +
        '","nome":"' +
        userName +
        '","fone":"' +
        userPhone +
        '","email":"' +
        null +
        '","agendamento":"' +
        daySel +
        " " +
        hourSel +
        '"}'
    );
    req.end();
  }

  render() {
    const { ...dateinfo } = this.state;
    const { ...useinfos } = dateinfo.data;

    const { success } = this.state;

    const { userName, userPhone, daySel, hourSel } = this.state;

    if (
      userName !== "" &&
      userPhone !== "" &&
      daySel !== "" &&
      hourSel !== ""
    ) {
      var validation = true;
    }

    const hourList = Object.keys(useinfos).map(function(key, i) {
      return (
        <option key={i} value={String(useinfos[key])}>
          {useinfos[key]}
        </option>
      );
    });

    return (
      <root.div className="container-inner container-schedule ">
        {success === false ? (
          myVariables[1] === null || myVariables[1] === "" ? (
            ""
          ) : (
            <div className="tab-schedule">
              <p className="tab-title">
                Estamos fora do expediente agende sua ligação.
              </p>

              <input
                className="tab-input__name-hour"
                type="text"
                placeholder="Nome"
                onInput={e => this.getName(e)}
              />
              <InputMask
                className="tab-input__phone-hour"
                mask="(99)99999-9999"
                maskChar=" "
                placeholder="Telefone"
                onInput={e => this.getPhone(e)}
              />

              <select
                className="tab-input__hour"
                value={this.state.value}
                onChange={this.daySelect}
              >
                <option value="" disabled selected>
                  Selecione a data
                </option>
                <option value={String(dateinfo.tomorrow)}>
                  Amanhã: {dateinfo.tomorrow}
                </option>
                <option value={String(dateinfo.dayone)}>
                  Dia: {dateinfo.dayone}
                </option>
                <option value={String(dateinfo.daytwo)}>
                  Dia: {dateinfo.daytwo}
                </option>
              </select>

              <select
                className="tab-input__date"
                value={this.state.value}
                onChange={this.hourSelect}
              >
                <option value="" disabled selected>
                  Selecione uma hora
                </option>
                {hourList}
              </select>

              <div className="tab-button">
                {validation ? (
                  <button
                    className="tab-button__button"
                    onClick={this.userSchedule}
                  >
                    Agendar
                  </button>
                ) : (
                  <button className="tab-button__button--disabled">
                    Agendar
                  </button>
                )}
              </div>
              <style type="text/css">{styles}</style>
            </div>
          )
        ) : myVariables[1] === null || myVariables[1] === "" ? (
          ""
        ) : (
          <div className="tab-schedule">
            <SuccessTab />
            <style type="text/css">{styles}</style>
          </div>
        )}
      </root.div>
    );
  }
}
