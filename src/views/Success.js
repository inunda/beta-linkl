import React from "react";
import root from "react-shadow";
import styles from "!raw-loader!../styled.css";

export default class SuccessTab extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <root.div className="container-inner">
        <div className="success-wrap">
          <img
            className="success-img"
            src="http://linkl.com.br/widget/img/check-circle.png"
            alt="Success"
          />
          <p className="success-p">Enviado com sucesso!</p>
        </div>
        <style type="text/css">{styles}</style>
      </root.div>
    );
  }
}
