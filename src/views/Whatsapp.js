import React from "react";
import root from "react-shadow";
import styles from "!raw-loader!../styled.css";
import SuccessTab from "./Success";

export default class WhatsTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      usermsg: "",
      userwhats: this.props.userwhats,
      success: false,
      valid: false
    };
    this.getUserMsg = this.getUserMsg.bind(this);
    this.sendWhats = this.sendWhats.bind(this);
  }

  _isMobile() {
    var isMobile = /iphone|ipod|android|ie|blackberry|fennec/.test(
      navigator.userAgent.toLowerCase()
    );
    return isMobile;
  }

  getUserMsg(e) {
    const userMsg = e.target.value;

    if (userMsg !== null || userMsg !== "") {
      this.setState({
        usermsg: userMsg,
        valid: true
      });
    } else {
      this.setState({
        valid: false
      });
    }
  }

  sendWhats() {
    this.setState({
      success: true
    });
    setTimeout(
      function() {
        this.setState({
          success: false
        });
      }.bind(this),
      2000
    );
  }

  render() {
    const { usermsg, userwhats, success, valid } = this.state;

    return (
      <root.div className="container-inner container-whatsapp ">
        {success === false ? (
          <div className="tab__whatsapp">
            <p className="tab-title">
              Envie uma mensagem através do Whatsapp.{" "}
            </p>

            <textarea
              className="tab-whatsapp__textarea"
              placeholder="Digite sua mensagem ..."
              onInput={e => this.getUserMsg(e)}
            />

            <div className="tab-button">
              {!this._isMobile() ? (
                valid ? (
                  <a
                    href={
                      `https://web.whatsapp.com/send?phone=${userwhats}&text=` +
                      `${usermsg}`
                    }
                    target="_blank"
                    rel="noopener"
                    className="tab-button__button--a"
                    onClick={this.sendWhats}
                  >
                    Enviar mensagem
                  </a>
                ) : (
                  <p className="tab-button__button__a--disable">
                    Enviar mensagem
                  </p>
                )
              ) : valid ? (
                <a
                  href={
                    valid
                      ? `https://api.whatsapp.com/send?phone=${userwhats}&text=` +
                        `${usermsg}`
                      : ""
                  }
                  target="_blank"
                  rel="noopener"
                  className={
                    valid
                      ? "tab-button__button--a"
                      : "tab-button__button__a--disable"
                  }
                  onClick={valid ? this.sendWhats : ""}
                >
                  Enviar mensagem
                </a>
              ) : (
                <p className="tab-button__button__a--disable">
                  Enviar mensagem
                </p>
              )}
            </div>
            <style type="text/css">{styles}</style>
          </div>
        ) : (
          <div className="tab__whatsapp">
            <SuccessTab />
          </div>
        )}
      </root.div>
    );
  }
}
