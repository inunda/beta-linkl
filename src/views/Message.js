import React from "react";
import root from "react-shadow";
import styles from "!raw-loader!../styled.css";
import validator from "validator";

import http from "https";
import { linklmsg } from "../utils/Apis";
import { myVariables } from "../utils/GlobalVariables";

import InputMask from "react-input-mask";
import SuccessTab from "./Success";

export default class MessageTab extends React.Component {
  constructor() {
    super();
    this.state = {
      userName: "",
      userPhone: "",
      userEmail: "",
      userMessage: "",
      success: false,
      valid: false,
      test: false
    };

    this.getName = this.getName.bind(this);
    this.getPhone = this.getPhone.bind(this);
    this.getEmail = this.getEmail.bind(this);
    this.getMessage = this.getMessage.bind(this);
    this.userMessage = this.userMessage.bind(this);
    this.validFunc = this.validFunc.bind(this);
  }

  getName(e) {
    const getUserName = e.target.value;

    this.setState({
      userName: getUserName
    });

    this.validFunc();
  }

  getPhone(e) {
    const getUserPhone = e.target.value;
    const userPhone = getUserPhone
      .replace(/[{()}]/g, "")
      .replace("-", "")
      .replace(" ", "")
      .replace("  ", "");

    if (userPhone > 11) {
      this.setState({
        userPhone
      });
    } else {
      this.setState({
        valid: false
      });
    }

    this.validFunc();
  }

  getEmail(e) {
    const getUserEmail = e.target.value;

    if (validator.isEmail(getUserEmail) && !validator.isEmpty(getUserEmail)) {
      this.setState({
        userEmail: getUserEmail
      });
    } else {
      this.setState({
        valid: false
      });
    }
    this.validFunc();
  }

  getMessage(e) {
    const getUserMessage = e.target.value;

    this.setState({
      userMessage: getUserMessage
    });

    this.validFunc();
  }

  validFunc() {
    const { userName, userPhone, userEmail, userMessage } = this.state;
    if (
      userName === "" ||
      userPhone === "" ||
      userEmail === "" ||
      userMessage === ""
    ) {
      this.setState({
        valid: false
      });
    } else {
      this.setState({
        valid: true
      });
    }
  }

  userMessage() {
    const { userName, userPhone, userEmail, userMessage } = this.state;

    var req = http.request(
      linklmsg,
      function(res) {
        var chunks = [];

        res.on("data", function(chunk) {
          chunks.push(chunk);
        });

        res.on(
          "end",
          function() {
            this.setState({
              success: true
            });
            setTimeout(
              function() {
                this.setState({
                  success: false
                });
              }.bind(this),
              2000
            );
          }.bind(this)
        );
      }.bind(this)
    );

    req.write(
      '{"codigo":"' +
        myVariables[1] +
        '","nome":"' +
        userName +
        '","fone":"' +
        userPhone +
        '","email":"' +
        userEmail +
        '","mensagem":"' +
        userMessage +
        '"}'
    );
    req.end();
  }

  render() {
    const { success, valid } = this.state;
    return (
      <root.div className="container-inner container-msg">
        {success === false ? (
          <div className="tab-msg">
            <p className="tab-title">
              Deixe sua mensagem e retornaremos em breve.
            </p>
            <input
              className="tab-input__name-msg"
              type="text"
              placeholder="Nome"
              onInput={e => this.getName(e)}
            />
            <InputMask
              className="tab-input__phone-msg"
              mask="(99) 99999 - 9999"
              maskChar=" "
              placeholder="Telefone"
              onInput={e => this.getPhone(e)}
            />
            <input
              className="tab-input__email-msg"
              maskChar=" "
              placeholder="Email"
              onInput={e => this.getEmail(e)}
            />
            <textarea
              className="tab-input__text-msg"
              placeholder="Mensagem ..."
              onInput={e => this.getMessage(e)}
            />

            {valid === false ? (
              <button className="tab-button__buttonmsg--disabled" disabled>
                Ligar
              </button>
            ) : (
              <button
                className="tab-button__buttonmsg"
                onClick={this.userMessage}
              >
                Ligar
              </button>
            )}

            <style type="text/css">{styles}</style>
          </div>
        ) : (
          <div className="tab-msg">
            <SuccessTab />
          </div>
        )}
      </root.div>
    );
  }
}
