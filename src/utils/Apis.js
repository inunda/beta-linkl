export const linklcall = {
  method: "POST",
  hostname: ["app.linkl.com.br"],
  path: ["/apis/apiLigueme.php"],
  headers: {}
};

export const linklmsg = {
  method: "POST",
  hostname: ["app.linkl.com.br"],
  path: ["/apis/apiMensagem.php"],
  headers: {}
};

export const linklwhats = {
  method: "POST",
  hostname: ["app.linkl.com.br"],
  path: ["/apis/apiWhatsApp.php"],
  headers: {}
};

export const validFunc = {
  method: "POST",
  hostname: ["app.linkl.com.br"],
  path: ["/apis/apiValidacoesWidget.php"],
  headers: {}
};

export const userStatusVal = {
  method: "POST",
  hostname: ["app.linkl.com.br"],
  path: ["/apis/apiStatusCliente.php"],
  headers: {}
};

export const options = {
  method: "GET",
  hostname: ["worldclockapi.com"],
  path: ["/api/json/utc/now"],
  headers: {}
};
